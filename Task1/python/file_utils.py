import os
import sys

if len(sys.argv) < 3 or not sys.argv[1] or not sys.argv[2]:
    print("Invalid input parameters.")
    sys.exit(1)

dir1 = sys.argv[1]
dir2 = sys.argv[2]

if not os.path.isdir(dir1):
    print(f"{dir1} is not a valid directory.")
    sys.exit(1)

if not os.path.isdir(dir2):
    print(f"{dir2} is not a valid directory.")
    sys.exit(1)

unique_file_names = set()
unique_file_names.update(os.listdir(dir1))
unique_file_names.update(os.listdir(dir2))

if not unique_file_names:
    print("There are no files in both directories.")
    sys.exit(0)

print("Unique file names:")
for file_name in sorted(unique_file_names):
    print(file_name)
