param(
    [string]$dir1,
    [string]$dir2
)

if (-not $dir1 -or -not $dir2) {
    Write-Host "Invalid input parameters."
    exit
}

if (-not (Test-Path $dir1 -PathType Container)) {
    Write-Host "$dir1 is not a valid directory."
    exit
}

if (-not (Test-Path $dir2 -PathType Container)) {
    Write-Host "$dir2 is not a valid directory."
    exit
}

$uniqueFileNames = @{}

Get-ChildItem -Path $dir1 | ForEach-Object {
    $uniqueFileNames[$_.Name] = $true
}

Get-ChildItem -Path $dir2 | ForEach-Object {
    $uniqueFileNames[$_.Name] = $true
}

if ($uniqueFileNames.Count -eq 0) {
    Write-Host "There are no files in both directories."
    exit
}

$uniqueFileNames = $uniqueFileNames.Keys | Sort-Object

Write-Host "Unique file names:"
$uniqueFileNames | ForEach-Object {
    Write-Host $_
}
