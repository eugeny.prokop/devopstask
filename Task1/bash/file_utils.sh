#!/bin/bash

dir1=$1
dir2=$2

if [ -z "$dir1" ] || [ -z "$dir2" ]; then
    echo "Invalid input parameters."
    exit 1
fi

if [ ! -d "$dir1" ]; then
    echo "$dir1 is not a valid directory."
    exit 1
fi

if [ ! -d "$dir2" ]; then
    echo "$dir2 is not a valid directory."
    exit 1
fi

declare -A uniqueFileNames

while IFS= read -r -d '' file; do
    filename=$(basename -- "$file")
    uniqueFileNames["$filename"]=1
done < <(find "$dir1" -maxdepth 1 -type f -print0)

while IFS= read -r -d '' file; do
    filename=$(basename -- "$file")
    uniqueFileNames["$filename"]=1
done < <(find "$dir2" -maxdepth 1 -type f -print0)

if [ ${#uniqueFileNames[@]} -eq 0 ]; then
    echo "There are no files in both directories."
    exit 0
fi

sortedFiles=$(for file in "${!uniqueFileNames[@]}"; do echo "$file"; done | sort)

echo "Unique file names:"
echo "$sortedFiles"
