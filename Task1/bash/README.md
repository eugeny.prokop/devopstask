## Як запустити:

1. Перейдіть до каталогу з файлом file_utils.sh ввівши команду:
```
cd /task1
```
Замініть /task1 на реальний шлях до каталогу з вашим файлом file_utils.sh.

2. Переконайтеся, що у файла є права на виконання, якщо ні - введіть команду:
```
chmod +x file_utils.sh
```

3. Запустіть скрипт, передавши аргументи командного рядка - шляхи до директорій. Наприклад:
```
./file_utils.sh /path/to/dir1 /path/to/dir2
```
Замініть /path/to/dir1 і /path/to/dir2 на реальні шляхи до ваших директорій.

4. У результаті скрипт має вивести в консоль список унікальних імен файлів, знайдених в обох директоріях.  
Якщо не було знайдено жодного файлу в обох директоріях - буде виведено відповідне повідомлення.  
Якщо в процесі сталася помилка - буде виведено відповідне повідомлення про помилку.
