import java.io.File;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class FileUtils {
    public static void main(String[] args) {
        if (args.length < 2 || args[0] == null || args[1] == null) {
            System.out.println("Invalid input parameters.");
            return;
        }

        Set<String> uniqueFileNames = new TreeSet<>();
        File dir1 = new File(args[0]);
        if (dir1.isDirectory()) {
            Collections.addAll(uniqueFileNames, dir1.list());
        } else {
            System.out.println(args[0] + " is not a valid directory.");
            return;
        }

        File dir2 = new File(args[1]);
        if (dir2.isDirectory()) {
            Collections.addAll(uniqueFileNames, dir2.list());
        } else {
            System.out.println(args[1] + " is not a valid directory.");
            return;
        }

        if (uniqueFileNames.isEmpty()) {
            System.out.println("There are no files in both directories.");
            return;
        }

        System.out.println("Unique file names:");
        for (String fileName : uniqueFileNames) {
            System.out.println(fileName);
        }
    }
}
