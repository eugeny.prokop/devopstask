```
unit tests (quick)
code linters
release new package version in registry of packages
build code
create git tag for current branch
publish static content for preview (static content is creating when run specific command for generate it)
```

**1. Unit Tests (Quick) + Code Linters**  
Ці кроки об'єднуємо, оскільки вони призначені для перевірки правильної роботи (юніт-тести) і якості/стандартів коду (лінтери).
Їх потрібно запускати першими, оскільки вони надають швидкий зворотний зв'язок про якість та правильну роботу коду і допомагають виявити проблеми на ранніх етапах розробки.

**2. Build Code**  
Після успішного проходження юніт-тестів і перевірки лінтерів, можна приступити до збірки додатку.

**3. Create git tag for current branch + Release new package version in registry of packages**  
Після успішного проходження тестів і збірки, створення нового тега в сховищі Git і створення нової версії пакета в реєстрі пов'язані з однією конкретною версією програмного забезпечення, тому логічно їх об'єднати.

**4. Publish static content for preview (static content is creating when run specific command for generate it)**  
Публікація статичного контенту для попереднього перегляду важлива для швидкої та зручної перевірки змін, що вносяться в додаток. 
